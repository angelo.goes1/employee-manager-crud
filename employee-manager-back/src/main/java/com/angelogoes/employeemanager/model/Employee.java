package com.angelogoes.employeemanager.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.Table;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Column;

@Data
@Entity
//@Table(name = "employee")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    private String name;
    private String email;
    private String jobTitle;
    private String phone;
    private String imageUrl;

    @Column(nullable = false, updatable = false)
    private String employeeCode;

    @Override
    public String toString() {
        return "Employee {" + 
                "id=" + id + 
                ", name=\'" + name + "\'" +
                ", email=\'" + email + "\'" +
                ", jobTitle=\'" + jobTitle + "\'" +
                ", phone=\'" + phone + "\'" +
                ", imageUrl=\'" + imageUrl + "\'" +
                "}";
    }
}
